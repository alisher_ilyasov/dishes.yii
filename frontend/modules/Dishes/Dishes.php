<?php

namespace frontend\modules\Dishes;

/**
 * Dishes module definition class
 */
class Dishes extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\Dishes\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
