<?php

namespace frontend\modules\Dishes\controllers;
use frontend\modules\Dishes\models\Dish;
use frontend\modules\Dishes\models\SearchForm;
use yii\web\Controller;
use yii;

/**
 * Default controller for the `Dishes` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $dishes = [];

        $model = new SearchForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            /**
             * Проверка на полное совпадение игредиентов
             */
            $sql = 'SELECT * from
                      (SELECT d.*, count(i.name) ingredients, count(i2.name) ingredients_limited
                       FROM dish d
                       LEFT JOIN dishes_ingredients di ON di.dish_id = d.id
                       LEFT JOIN ingredients i ON i.id = di.ingredient_id
                       LEFT JOIN ingredients i2 ON i2.id = di.ingredient_id
                       AND i2.id IN ('. implode(',', $model->ingredients_ids) .')
                       AND i.id=i2.id
                       AND d.state = '. Dish::STATUS_ACTIVE .'
                       AND d.id NOT IN ('. implode(',', Dish::getIgnoredDishIds()) .')
                       GROUP BY d.id) iids
                    WHERE iids.ingredients = iids.ingredients_limited';
            $data = Dish::findBySql($sql)->all();
            if($data){
                $dishes = $data;
            } else {
                /**
                 * Если полное совпадение не нашлось, ищем частичное не меньше 2х ингредиентов
                 */
                $dishes = Dish::find()
                    ->joinWith('ingredients')
                    ->where(['NOT IN', 'dish.id', Dish::getIgnoredDishIds()])
                    ->andWhere(['ingredients.id' => $model->ingredients_ids])
                    ->andWhere(['`dish`.`state`' => Dish::STATUS_ACTIVE])
                    ->groupBy('id')
                    ->orderBy(['COUNT(*)' => SORT_DESC])
                    ->andHaving('COUNT(*) >= 2')
                    ->all();

                if(!$dishes)
                    Yii::$app->getSession()->setFlash('dish_search_error', 'Ничего не найдено.');
            }
        }

        return $this->render('index',
            [
                'model' => $model,
                'dishes' => $dishes
            ]
        );
    }
}
