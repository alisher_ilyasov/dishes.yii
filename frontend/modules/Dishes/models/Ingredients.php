<?php

namespace frontend\modules\Dishes\models;

use Yii;

/**
 * This is the model class for table "{{%ingredients}}".
 *
 * @property int $id
 * @property string $name Наименование ингредиента
 * @property int $state Доступен
 * @property string $create_time Дата создания
 * @property string $update_time Дата обновления
 * @property int $author_id Автор
 *
 * @property DishesIngredients[] $dishesIngredients
 */
class Ingredients extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ingredients}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'state'], 'required'],
            [['state', 'author_id'], 'integer'],
            [['create_time', 'update_time'], 'safe'],
            [['name'], 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование ингредиента',
            'state' => 'Доступен',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата обновления',
            'author_id' => 'Автор',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDishesIngredients()
    {
        return $this->hasMany(DishesIngredients::className(), ['ingredient_id' => 'id']);
    }
}
