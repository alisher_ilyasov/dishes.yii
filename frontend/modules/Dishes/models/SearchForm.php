<?php
/**
 * Created by PhpStorm.
 * User: Алишер
 * Date: 03.10.2018
 * Time: 0:33
 */

namespace frontend\modules\Dishes\models;
use yii\base\Model;

class SearchForm extends Model
{
    public $ingredients_ids;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ingredients_ids'], 'required'],
            [['ingredients_ids'], 'each', 'rule' => ['integer']],
            ['ingredients_ids', function ($attribute, $params) {
                if (count($this->$attribute) < 2) {
                    $this->addError($attribute, 'Выберите больше ингредиентов');
                } else if (count($this->$attribute) >5)
                    $this->addError($attribute, 'Вы выбрали слишком много ингредиентов');
            }]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ingredients_ids' => 'Ингредиенты блюда'
        ];
    }
}