<?php

namespace frontend\modules\Dishes\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%dish}}".
 *
 * @property int $id
 * @property string $name Наименование блюда
 * @property string $description Описание блюда
 * @property int $state Доступно
 * @property string $create_time Дата создания
 * @property string $update_time Дата изменения
 * @property int $author_id Автор
 *
 * @property DishesIngredients[] $dishesIngredients
 */
class Dish extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%dish}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'state'], 'required'],
            [['description'], 'string'],
            [['create_time', 'update_time'], 'safe'],
            [['author_id'], 'integer'],
            [['name'], 'string', 'max' => 512],
            [['state'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование блюда',
            'description' => 'Описание блюда',
            'state' => 'Доступно',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата изменения',
            'author_id' => 'Автор',
        ];
    }

    /**
     * Получение ID блюд у которых один из ингредиентов отключен
     * @return array
     */
    public static function getIgnoredDishIds()
    {
        $ignoredDishesIds = parent::find()->joinWith('ingredients')
            ->where(
                [
                    'ingredients.state' => Ingredients::STATUS_INACTIVE,
                ]
            )
            ->select(['dish.id'])
            ->groupBy(['dish.id'])
            ->all();

        if ($ignoredDishesIds)
            return ArrayHelper::map($ignoredDishesIds, 'id', 'id');

        return [0];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDishesIngredients()
    {
        return $this->hasMany(DishesIngredients::className(), ['dish_id' => 'id']);
    }

    public function getIngredients()
    {
        return $this->hasMany(
            Ingredients::className(),
            ['id' => 'ingredient_id']
        )->viaTable(
            'dishes_ingredients',
            ['dish_id' => 'id']
        );
    }
}
