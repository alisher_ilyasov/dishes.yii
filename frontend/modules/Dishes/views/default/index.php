<?php
/**
 * @var $dish \frontend\modules\Dishes\models\Dish
 */
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Html;
    use yii\widgets\Pjax;
    use yii\bootstrap\Alert;
    use yii\helpers\ArrayHelper;
    use frontend\modules\Dishes\models\Ingredients;
?>
<div class="container">
    <div class="row">
        <?php \yii\widgets\Pjax::begin(); ?>
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <h1>Поиск блюд по ингредиентам</h1>
            <div class="panel panel-default">
                <div class="panel-heading">Выберите несколько ингредиентов для поиска блюда</div>
                <div class="panel-body">
                    <?php $form = ActiveForm::begin([
                        'options' => ['data' => ['pjax' => true]],
                    ]); ?>
                    <?= $form->field($model, 'ingredients_ids')
                        ->dropDownList(
                                ArrayHelper::map(Ingredients::find()->all(), 'id', 'name'),
                                ['multiple' => true]
                        )
                    ?>
                    <div class="form-group">
                        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            <h1>Результат поиска:</h1>
            <?php
            if(Yii::$app->session->hasFlash('dish_search_error')):
                echo Alert::widget([
                    'options' => [
                        'class' => 'alert-danger',
                    ],
                    'body' => Yii::$app->session->getFlash('dish_search_error'),
                ]);
            endif;
            ?>
            <?php foreach ($dishes as $dish):?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= $dish->name ?></h3>
                </div>
                <div class="panel-body">
                    <?= $dish->description ?>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
        <?php Pjax::end(); ?>
        <div class="col-md-2"></div>
    </div>
</div>