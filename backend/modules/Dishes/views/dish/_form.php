<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\Dishes\models\Ingredients;

/* @var $this yii\web\View */
/* @var $model backend\modules\Dishes\models\Dish */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dish-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ingredients_ids')
        ->dropDownList(
            ArrayHelper::map(
                    Ingredients::find()
                        ->where(['state' => Ingredients::STATUS_ACTIVE])
                        ->all(), 'id', 'name'
            ),
            ['multiple' => true]
        ) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'state')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
