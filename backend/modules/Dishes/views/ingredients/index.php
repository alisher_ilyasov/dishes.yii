<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\Dishes\models\Ingredients;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\Dishes\models\IngredientsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ингредиенты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ingredients-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить ингредиент', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Вернуться на главную панель', ['/dishes'], ['class' => 'btn btn-warning']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'attribute' => 'state',
                'label' => 'Статус',
                'format' => 'html',
                'value' => function ($model) {
                    if ($model->state === 0) {
                        $class = 'danger';
                        $label = 'Скрыто';
                    } else {
                        $class = 'success';
                        $label = 'Видимо';
                    }
                    return '<span class="label label-' . $class . '">' . $label . '</span>';
                },
                'filter' => [
                    Ingredients::STATUS_ACTIVE => 'Видим',
                    Ingredients::STATUS_INACTIVE => 'Скрыт'
                ]
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
