<?php
use yii\helpers\Html;
?>
<div class="Dishes-default-index">
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <h1 style="text-align: center">Модуль администрирования блюд</h1>
                <div class="panel panel-default" style="margin:40px 0 0 0">
                    <div class="panel-body">
                        <p>Для начала работы выберите один из разделов панели администрирования.</p>
                        <p>
                            <?= Html::a('Список блюд', ['/dishes/dish'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Список ингредиентов', ['/dishes/ingredients'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</div>
