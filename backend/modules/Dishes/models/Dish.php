<?php

namespace backend\modules\Dishes\models;

use voskobovich\linker\LinkerBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\ExpressionBuilder;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%dish}}".
 *
 * @property int $id
 * @property string $name Наименование блюда
 * @property string $description Описание блюда
 * @property int $state Доступно
 * @property string $create_time Дата создания
 * @property string $update_time Дата изменения
 * @property int $author_id Автор
 *
 * @property DishesIngredients[] $dishesIngredients
 */
class Dish extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%dish}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_time',
                'updatedAtAttribute' => 'update_time',
                'value' => new Expression('NOW()')
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'author_id',
                'updatedByAttribute' => 'author_id',
            ],
            [
                'class' => LinkerBehavior::className(),
                'relations' => [
                    'ingredients_ids' => 'ingredients',
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'state'], 'required'],
            [['description'], 'string'],
            [['create_time', 'update_time'], 'safe'],
            [['author_id'], 'integer'],
            [['name'], 'string', 'max' => 512],
            [['state'], 'string', 'max' => 1],
            [['ingredients_ids'], 'each', 'rule' => ['integer']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование блюда',
            'description' => 'Описание блюда',
            'state' => 'Доступно',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата изменения',
            'author_id' => 'Автор',
            'ingredients_ids' => 'Ингридиенты блюда'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDishesIngredients()
    {
        return $this->hasMany(DishesIngredients::className(), ['dish_id' => 'id']);
    }

    public function getIngredients()
    {
        return $this->hasMany(
            Ingredients::className(),
            ['id' => 'ingredient_id']
        )->viaTable(
            'dishes_ingredients',
            ['dish_id' => 'id']
        );
    }
}
