<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%ingredients}}`.
 */
class m181003_205535_create_table_ingredients extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%ingredients}}', [

            'id' => $this->primaryKey()->notNull(),
            'name' => $this->string(512)->notNull()->comment("Наименование ингредиента"),
            'state' => $this->integer(11)->notNull()->comment("Доступен"),
            'create_time' => $this->datetime()->notNull()->comment("Дата создания"),
            'update_time' => $this->datetime()->notNull()->comment("Дата обновления"),
            'author_id' => $this->integer(11)->notNull()->comment("Автор"),

        ]);
     }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%ingredients}}');
    }
}
