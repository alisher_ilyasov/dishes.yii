<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%dishes_ingredients}}`.
 */
class m181003_205535_create_table_dishes_ingredients extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%dishes_ingredients}}', [

            'id' => $this->primaryKey()->notNull(),
            'dish_id' => $this->integer(11)->notNull(),
            'ingredient_id' => $this->integer(11)->notNull(),

        ]);
 
        // creates index for column `dish_id`
        $this->createIndex(
            'dishes_ingredients_ibfk_1',
            '{{%dishes_ingredients}}',
            'dish_id'
        );

        // add foreign key for table `dish`
        $this->addForeignKey(
            'dishes_ingredients_ibfk_1',
            '{{%dishes_ingredients}}',
            'dish_id',
            '{{%dish}}',
            'id',
            'CASCADE'
        );

        // creates index for column `ingredient_id`
        $this->createIndex(
            'dishes_ingredients_ibfk_2',
            '{{%dishes_ingredients}}',
            'ingredient_id'
        );

        // add foreign key for table `ingredients`
        $this->addForeignKey(
            'dishes_ingredients_ibfk_2',
            '{{%dishes_ingredients}}',
            'ingredient_id',
            '{{%ingredients}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `dish`
        $this->dropForeignKey(
            'dishes_ingredients_ibfk_1',
            '{{%dishes_ingredients}}'
        );

        // drops index for column `dish_id`
        $this->dropIndex(
            'dishes_ingredients_ibfk_1',
            '{{%dishes_ingredients}}'
        );

        // drops foreign key for table `ingredients`
        $this->dropForeignKey(
            'dishes_ingredients_ibfk_2',
            '{{%dishes_ingredients}}'
        );

        // drops index for column `ingredient_id`
        $this->dropIndex(
            'dishes_ingredients_ibfk_2',
            '{{%dishes_ingredients}}'
        );

        $this->dropTable('{{%dishes_ingredients}}');
    }
}
