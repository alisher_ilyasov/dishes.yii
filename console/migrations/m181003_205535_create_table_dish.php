<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%dish}}`.
 */
class m181003_205535_create_table_dish extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%dish}}', [

            'id' => $this->primaryKey()->notNull(),
            'name' => $this->string(512)->notNull()->comment("Наименование блюда"),
            'description' => $this->text()->notNull()->comment("Описание блюда"),
            'state' => $this->tinyInteger(1)->notNull()->comment("Доступно"),
            'create_time' => $this->timestamp()->notNull()->comment("Дата создания"),
            'update_time' => $this->timestamp()->notNull()->comment("Дата изменения"),
            'author_id' => $this->integer(11)->notNull()->comment("Автор"),

        ]);
     }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%dish}}');
    }
}
